from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

mud_src = "mud/src/"

src = [
    "mud.pyx",
    mud_src + "mud.c",
    mud_src + "mud_gen.c",
    mud_src + "mud_fort.c",
    mud_src + "mud_encode.c",
    mud_src + "mud_all.c",
    mud_src + "mud_tri_ti.c",
    mud_src + "mud_misc.c",
    mud_src + "mud_new.c",
]

inc = ["mud/src/"]

ext = Extension("mud", sources=src, include_dirs=inc)

setup(ext_modules=cythonize(ext))
