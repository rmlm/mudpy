
import mud as MUD

class mudpy:

   def __init__(self, filename):
      self.file_handle = None
      self.ptype = None
      self.n_scalers = None
      self.n_histograms = None
      self.n_independent_variables = None
      
      self.filename = filename
      
      self.histograms = []
      self.independent_variables = []
      self.scalers = []
      
      # open the MUD file
      self.file_handle = MUD.open_read(self.filename)
      
      # read the blocks of data      
      self.__get_description()
      self.__get_scalers()
      self.__get_histograms()
      self.__get_independent_variables()
      
      # close the MUD file
      MUD.close_read(self.file_handle)
      
   
   # get the run description from the MUD file  
   def __get_description(self):
      MUD.get_description(self.file_handle)
      self.experiment_number = MUD.get_exp_number(self.file_handle)
      self.run_number =  MUD.get_run_number(self.file_handle)
      self.elapsed_seconds =  MUD.get_elapsed_seconds(self.file_handle)
      self.start_time =  MUD.get_start_time(self.file_handle)
      self.end_time =  MUD.get_end_time(self.file_handle)
      self.title = MUD.get_title(self.file_handle)
      self.lab =  MUD.get_lab(self.file_handle)
      self.area =  MUD.get_area(self.file_handle)
      self.method =  MUD.get_method(self.file_handle)
      self.apparatus =  MUD.get_apparatus(self.file_handle)
      self.insert =  MUD.get_insert(self.file_handle)
      self.sample =  MUD.get_sample(self.file_handle)
      self.orientation =  MUD.get_orientation(self.file_handle)
      self.das =  MUD.get_das(self.file_handle)
      self.experimenter = MUD.get_experimenter(self.file_handle)

   # get the run scalers from the MUD file  
   def __get_scalers(self):
      self.ptype, self.n_scalers = MUD.get_scalers(self.file_handle)
      # loop over all histograms (indexing goes from 1 to n)
      for i in range(1, self.n_scalers + 1, 1):
         # make an empty raw scaler data structure
         rs = raw_scaler()
         # fill it with the data
         rs.label = MUD.get_scaler_label(self.file_handle, i)
         rs.counts = MUD.get_scaler_counts(self.file_handle, i)
         # append it to the list of scalers
         self.scalers.append(rs)
         

   # get the histograms from the MUD file  
   def __get_histograms(self):
      self.ptype, self.n_histograms = MUD.get_hists(self.file_handle)
      # loop over all histograms (indexing goes from 1 to n)
      for i in range(1, self.n_histograms + 1, 1):
         # make an empty raw histogram data structure
         rh = raw_histogram()
         # fill it with the data
         rh.id = i
         rh.type = MUD.get_hist_type(self.file_handle, i)
         rh.n_bytes = MUD.get_hist_n_bytes(self.file_handle, i)
         rh.n_bins = MUD.get_hist_n_bins(self.file_handle, i)
         rh.bytes_per_bin = MUD.get_hist_bytes_per_bin(self.file_handle, i)
         rh.fs_per_bin = MUD.get_hist_fs_per_bin(self.file_handle, i)
         rh.t0_ps = MUD.get_hist_t0_ps(self.file_handle, i)
         rh.t0_bin = MUD.get_hist_t0_bin(self.file_handle, i)
         rh.good_bin1 = MUD.get_hist_good_bin1(self.file_handle, i)
         rh.good_bin2 = MUD.get_hist_good_bin2(self.file_handle, i)
         rh.background1 = MUD.get_hist_background1(self.file_handle, i)
         rh.background2 = MUD.get_hist_background2(self.file_handle, i)
         rh.n_events = MUD.get_hist_n_events(self.file_handle, i)
         rh.title = MUD.get_hist_title(self.file_handle, i)
         rh.sec_per_bin = MUD.get_hist_sec_per_bin(self.file_handle, i) 
         rh.data = MUD.get_hist_data(self.file_handle, i)
         # append it to the list of histograms
         self.histograms.append(rh)
       
   # get the logged individual variables from the MUD file  
   def __get_independent_variables(self):
      self.ptype, self.n_independent_variables = MUD.get_ivars(self.file_handle)
      # loop over all independent variables (indexing goes from 1 to n)
      for i in range(1, self.n_independent_variables + 1, 1):
         # make an empty raw independent variable data structure
         riv = raw_independent_variable()
         # fill it with the data
         riv.id = i
         riv.title = MUD.get_ivar_name(self.file_handle, i)
         riv.description = MUD.get_ivar_description(self.file_handle, i)
         riv.units = MUD.get_ivar_units(self.file_handle, i)
         riv.low = MUD.get_ivar_low(self.file_handle, i)
         riv.high = MUD.get_ivar_high(self.file_handle, i)
         riv.mean = MUD.get_ivar_mean(self.file_handle, i)
         riv.standard_deviation = MUD.get_ivar_std(self.file_handle, i)
         riv.skewness = MUD.get_ivar_skewness(self.file_handle, i)
         # append it to the list of independent variables
         self.independent_variables.append(riv)


# container for raw histogram data from MUD file
class raw_histogram:
   pass

# container for raw independent variable data from MUD file
class raw_independent_variable:
   pass

# container for raw scaler data from MUD file
class raw_scaler:
   pass
