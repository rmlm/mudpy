# mudpy: a Python wrapper for the TRIUMF MUD library

`mudpy` is Python wrapper for the TRIUMF MUD library, written in C.
It a provides a friendlier interface to routines used to read MUD files, which
are used to store data taken in μSR and β-NMR experiments performed at
[TRIUMF](https://www.triumf.ca/). The wrapping is done using
[Cython](https://cython.org/).

This repo contains my fork of the routines orginally written by D. Fujimoto,
which can be found [here](https://github.com/dfujim/bdata).
Information about the MUD library, as well as its source code, can be found
[here](http://cmms.triumf.ca/mud/).

Example build instructions:
```
python3 setup.py build_ext --inplace
```
